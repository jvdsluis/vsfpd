### Changed
 - Moved hosting to gitlab.com/fpcprojects/vscode-fpdebug
### Removed
### Added
 - ForceNewConsoleWin option
### Fixed

## [0.4.0] - 2021-5-15
### Changed
 - Switched to PasSrc/cnocExpressionParser as default expression-parser
 - Updated to work with newer versions of Lazarus' FpDebug
### Added
 - Ability to evaluate fields and methods
 - Ability to evaluate indexed array-values
 - Ability to evaluate properties. But for now it is based on a few guesses,
   because fpc does not generate debug-information for properties. So for
	 now, when a field is not found:
	   - it is tried again, with a 'get' prefix
	   - still not found, and the name is 'Items', then 'getItem' is tried
	   - still not found, it is tried with a 'F' prefix
### Fixed
 - Fixed crash of debugger on invalid expressions

## [0.3.0] - 2020-12-31
### Changed
 - Speed improvement
 - Switched to another expression-evaluator, based on fcl-passrc
### Added
 - Ability to call simple functions on x86_64-linux
 - Ability to set the value of regvar variables on x86_64-linux
### Fixed
 - Several memory-leaks are solved
 - Fixed AV due to memory corruption related to the arguments-scope
 - Fixed a character-eating bug in the tcp/ip-DAB interface

## [0.2.2] - 2020-07-02
### Added
- Tree-view display of record- and class-members
- Tree-view display of array-elements
- Support progam-parameters, working-directory and environment

## [0.2.1] - 2020-06-13
### Added
- Rudimentary exception support
- This extension now also registers Pascal as a language. To make it possible to set breakpoints when no other Pascal extension is running.

### Changed
- Speed improvements